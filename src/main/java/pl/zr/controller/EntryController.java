package pl.zr.controller;

import org.springframework.stereotype.Component;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 * Created by Daniel on 2016-08-28.
 */
@Component
@ManagedBean
@SessionScoped
public class EntryController {

    public String getName() {
        return "Daniel Krasowski";
    }
}
